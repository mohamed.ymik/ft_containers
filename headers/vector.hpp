#ifndef VECTOR_H
# define VECTOR_H
# include <iostream>

namespace ft {
    template <class T, class Alloc = std::allocator<T> >
    class vector {
        public:
            typedef T value_type;
            typedef T& reference;
            typedef T* pointer;
            typedef T const& const_reference;
            typedef T const* const_pointer;
            typedef Alloc allocator_type;
            typedef size_t size_type;

            // Constructors
            explicit vector(allocator_type const& alloc = allocator_type()) {
                _cap = 0;
                _size = 0;
            }

            explicit vector(size_type n, const_reference val = value_type(),
            allocator_type const& alloc = allocator_type()) {

            }

            template <class InputIterator>
            vector(InputIterator first, InputIterator last,
            allocator_type const& alloc = allocator_type()) {
            }

            // Destructors
            // This destroys all container elements, and deallocates all the storage capacity allocated by the vector using its allocator.
            virtual ~vector() {}

            // Iterators

            /**
             * CAPACITY
             **/
            size_type capacity() const {
                return _cap;
            }

            size_type size() const {
                return _size;
            }

            size_type max_size() const {
                return _alloc.max_size();
            }

            bool empty() const {
                return _size == 0;
            }

            void resize (size_type n, value_type val = value_type()) {
                if (n < _size) {
                    _size = n;
                } else if (n > _size && n < _cap) {

                } else {
                    // Reallocation
                    pointer *_new = _alloc.allocate(n + 1);
                }
            }
        private:
            size_type _cap;
            size_type _size;
            allocator_type _alloc;
    };
}
#endif