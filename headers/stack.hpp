#ifndef STACK_HPP
# define STACK_HPP
# include <iostream>
# include <deque>

namespace ft {
    template <class T, class Container = std::deque<T> >
    class stack {
        public:
            typedef T value_type;
            typedef Container container_type;
            typedef size_t size_type;
            explicit stack(container_type const& ctnr = container_type()) : c(ctnr) {}

            bool empty() const {
                return c.size() == 0;
            }

            size_type size() const {
                return c.size();
            }

            value_type& top() {
                return c.back();
            }

            value_type const& top() const {
                return c.back();
            }

            void pop() {
                c.pop_back();
            }

            void push(value_type const& val) {
                c.push_back(val);
            }

            friend bool operator==(stack<T, Container> const& lhs, stack<T, Container> const& rhs) {
                return lhs.c == rhs.c; 
            }

            friend bool operator!=(stack<T, Container> const& lhs, stack<T, Container> const& rhs) {
                return lhs.c == rhs.c; 
            }

        private:
            container_type c;
    };
    template <class T, class Container>
    bool operator<(const stack<T,Container>& lhs, const stack<T,Container>& rhs) {
        return lhs < rhs;
    }

    template <class T, class Container>
    bool operator<= (const stack<T,Container>& lhs, const stack<T,Container>& rhs) {
        return lhs <= rhs;
    }

    template <class T, class Container>
    bool operator>  (const stack<T,Container>& lhs, const stack<T,Container>& rhs) {
        return lhs > rhs;
    }

    template <class T, class Container>
    bool operator>= (const stack<T,Container>& lhs, const stack<T,Container>& rhs) {
        return lhs >= rhs;
    }
}
#endif