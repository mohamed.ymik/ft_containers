#include "vector.hpp"
#include <iostream>
#include <vector>
#include <assert.h>

void     vector_test_capacity() {
    std::cout << "Test vector capacity: ";
    ft::vector<int> v;
    std::vector<int> v2;
    assert(v2.capacity() == v.capacity());
    std::cout << "OK" << std::endl;
}

void     vector_test_size() {
    std::cout << "Test vector size: ";
    ft::vector<int> v;
    std::vector<int> v2;
    assert(v2.size() == v.size());
    std::cout << "OK" << std::endl;
}

void     vector_test_max_size() {
    std::cout << "Test vector max size: ";
    ft::vector<int> v;
    std::vector<int> v2;
    assert(v2.max_size() == v.max_size());
    std::cout << "OK" << std::endl;
}

void     vector_test_empty() {
    std::cout << "Test vector empty: ";
    ft::vector<int> v;
    std::vector<int> v2;
    assert(v2.empty() == v.empty());
    std::cout << "OK" << std::endl;
}

void     vector_test()
{
    vector_test_capacity();
    vector_test_size();
    vector_test_max_size();
    vector_test_empty();
}