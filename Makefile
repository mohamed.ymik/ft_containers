SRCS_TESTS=tests/stack_test.cpp tests/main_test.cpp tests/vector_test.cpp
OBJS_TESTS=$(SRCS_TESTS:.cpp=.o)
CC=clang++
FLAGS=-Wall -Wextra -Werror

tests: $(OBJS_TESTS)
	@$(CC) $(OBJS_TESTS) -I headers -o test_prog
	@./test_prog
	@make clean_tests
%.o: %.cpp
	@$(CC) -c $< -o $@ -I headers

clean_tests:
	@rm -f $(OBJS_TESTS)
	@rm -f test_prog