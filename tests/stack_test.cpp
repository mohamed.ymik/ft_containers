#include "stack.hpp"
#include <iostream>
#include <cassert>

static void    test_stack_empty() {
	std::cout << "Test stack empty: ";
	ft::stack<int> s;
	assert(s.empty() == true);
	std::cout << "OK" << std::endl;
}

static void    test_stack_size() {
	std::cout << "Test stack size: ";
	ft::stack<int> s;
	assert(s.size() == 0);
	std::cout << "OK" << std::endl;
}

static void    test_stack_top() {
	std::cout << "Test stack top: ";
	ft::stack<int> s;
	s.push(1337);
	assert(s.top() == 1337);
	assert(s.size() == 1);
	std::cout << "OK" << std::endl;
}

static void    test_stack_push() {
	std::cout << "Test stack push: ";
	ft::stack<int> s;
	s.push(212);
	assert(s.top() == 212);
	assert(s.size() == 1);
	std::cout << "OK" << std::endl;
}

static void    test_stack_pop() {
	std::cout << "Test stack pop: ";
	ft::stack<int> s;
	s.push(212);
	s.push(213);
	s.pop();
	assert(s.top() == 212);
	assert(s.size() == 1);
	std::cout << "OK" << std::endl;
}

static void		test_stack_equals_operator() {
	std::cout << "Test stack == operator: ";
	std::deque<int> s;
	s.push_back(1);
	s.push_back(8);
	ft::stack<int> st(s);
	ft::stack<int> st2(s);
	assert(st == st2);
	std::cout << "OK" << std::endl;

}

static void		test_stack_not_equals_operator() {
	std::cout << "Test stack != operator: ";
	std::deque<int> s;
	s.push_back(1);
	s.push_back(8);
	ft::stack<int> st(s);
	ft::stack<int> st2(s);
	assert(st != st2);
	std::cout << "OK" << std::endl;
}


void	stack_test() {
	
	test_stack_empty();
	test_stack_size();
	test_stack_top();
	test_stack_push();
	test_stack_pop();
	test_stack_equals_operator();
	test_stack_not_equals_operator();
}